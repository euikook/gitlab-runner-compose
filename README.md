# Run Gitlab Runner in a container

## Prerequisites
* [Docker](https://www.docker.com/)
* [Docker Compose](https://docs.docker.com/compose/)

## Usages

###  Configuration

* Replace *CI_SERVER_URL* value with your Gitlab server e.g. https://gitlab.com
* [Obtain a token](https://docs.gitlab.com/ee/ci/runners/) for a shared or specific Runner via GitLab’s interface and replace *REGISTRATION_TOKEN* value

```
tee .env << END
CI_SERVER_URL=https://gitlab.com/
REGISTRATION_TOKEN=zDsz34JuZf95NoBaQPX
END
```

### Registration and run gitlab-runner

```
docker-compose up -d
```


# References
* https://docs.gitlab.com/ee/ci/runners/
* https://docs.gitlab.com/runner/install/docker.html
* https://dev.to/imichael/this-one-trick-gives-you-unlimited-ci-minutes-on-gitlab-e92
